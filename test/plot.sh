#!/usr/bin/gnuplot
set termoption enhanced
set terminal png size 1600,900 font "Arial, 18"
set output "Add.png"

set style line 1 lc rgb "0xDC143C" lt 1 lw 2 pt 1 ps 2
set style line 2 lc rgb "0x8072FA" lt 1 lw 2 pt 1 ps 2

set border lw 2
set grid
set key bottom right

set logscale y

set title "График сравнения выполнения операции добавления у Splay и B деревьев"  font "Arial, 18"
set xlabel "Elements" font "Arial, 18"
unset ylabel
set ylabel "Time, с" font "Arial, 18" offset 3.5,0 rotate by 0
set xrange [0:*]
set xtics font "Arial, 18"
set ytics font "Arial, 18"
set format y "10^{%T}"

plot "BTreeAdd.txt" u 1:2 smooth bezier, \
"SplayAdd.txt"  u 1:2 smooth bezier,



set output "Delete.png"

set border lw 2
set grid
set key bottom right

set logscale y

set title "График сравнения выполнения операции удаления у Splay и B деревьев"  font "Arial, 18"
set xlabel "Количество удалений рандомных элементов" font "Arial, 18"
unset ylabel
set ylabel "Time, с" font "Arial, 18" offset 3.5,0 rotate by 0
set ytics font "Arial, 18"
set format y "10^{%T}"

plot "BTreeDelete.txt" u 0:2 smooth bezier, \
"SplayDelete.txt" u 0:2 smooth bezier


set output "Search.png"

set border lw 2
set grid
set key bottom right

set logscale y

set title "График сравнения выполнения операции поиска у Splay и B деревьев"  font "Arial, 18"
set xlabel "Количество поисков рандомных элементов" font "Arial, 18"
unset ylabel
set ylabel "Time, с" font "Arial, 18" offset 3.5,0 rotate by 0
set ytics font "Arial, 18"
set format y "10^{%T}"

plot "BTreeSearch.txt" u 0:2 smooth bezier, \
"SplaySearch.txt" u 0:2 smooth bezier



set output "SearchMax.png"

set border lw 2
set grid
set key bottom right

set logscale y

set title "График сравнения выполнения операции поиска максимума у Splay и B деревьев"  font "Arial, 18"
set xlabel "Количество поисков максимального элемента" font "Arial, 18"
unset ylabel
set ylabel "Time, с" font "Arial, 18" offset 3.5,0 rotate by 0
set ytics font "Arial, 18"
set format y "10^{%T}"

plot "BTreeSearchMax.txt" u 0:2 smooth bezier, \
"SplaySearchMax.txt" u 0:2 smooth bezier

set output "SearchMin.png"

set border lw 2
set grid
set key bottom right

set logscale y

set title "График сравнения выполнения операции поиска минимума у Splay и B деревьев"  font "Arial, 18"
set xlabel "Количество поисков минимального элемента" font "Arial, 18"
unset ylabel
set ylabel "Time, с" font "Arial, 18" offset 3.5,0 rotate by 0
set ytics font "Arial, 18"
set format y "10^{%T}"

plot "BTreeSearchMin.txt" u 0:2 smooth bezier, \
"SplaySearchMin.txt" u 0:2 smooth bezier