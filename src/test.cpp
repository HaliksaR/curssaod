#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <string>
#include <fstream>
#include <chrono>
#include <vector>

#define MAX 2000000000
#define GAP 100000
#define MAXSEARCH 30000

#define RESET "\033[0m"
#define GREEN "\033[1;32m"
#define RED   "\033[1;31m"

#define COUTRED cout << RED
#define COUTGREEN cout << GREEN
#define ENDLRESET  RESET<<endl

#include "includes/SplayTree.h"
#include "includes/BTree.h"

class Timer {
public:
    Timer() : beg_(clock_::now()) {}

    void reset() { beg_ = clock_::now(); }

    double elapsed() const {
        return std::chrono::duration_cast<second_>
                (clock_::now() - beg_).count();
    }

private:
    typedef std::chrono::high_resolution_clock clock_;
    typedef std::chrono::duration<double, std::ratio<1> > second_;
    std::chrono::time_point<clock_> beg_;
};

void rendering(string data, string name) {
    ofstream file;
    file.open(name);
    if (!file) {
        COUTRED << "file not open: " + name << ENDLRESET;
        exit(-1);
    }
    file << "#      X         Y" << endl;
    file << data;
    file.close();
    COUTGREEN << "save in: " + name << ENDLRESET;
}

void calc(vector<double> array) {
    double max = INTMAX_MIN, min = INTMAX_MAX, mid = 0;
    for (int i = 0; i < array.size(); ++i) {
        if (array[i] != 0) {
            if (max < array[i]) {
                max = array[i];
            }
            if (min > array[i]) {
                min = array[i];
            }
            mid += array[i];
        }
    }
    mid /= array.size();
    cout << setprecision(10) << fixed << "max: " << max << " min: " << min << " mid: " << mid << endl;
}

vector<double> SplayAdd(SplayTree<int> &NewTree) {
    std::stringstream stringstream1;
    vector<double> array;
    for (int i = 0; i < MAX; i++) {
        if (i % GAP == 0) {
            Timer tmr;
            NewTree.insert(i, i);
            double t = tmr.elapsed();
            array.push_back(t);
            stringstream1 << "\t\t" << i << "\t\t\t" << setprecision(10) << fixed << t << endl;
            tmr.reset();
        }
    }
    rendering(stringstream1.str(), "../test/SplayAdd.txt");
    stringstream1.clear();
    return array;
}

vector<double> BTreeAdd(BTree &NewBTree) {
    std::stringstream stringstream1;
    vector<double> array;
    for (int i = 0; i < MAX; i++) {
        if (i % GAP == 0) {
            Timer tmr;
            NewBTree.insert(i);
            double t = tmr.elapsed();
            array.push_back(t);
            stringstream1 << "\t\t" << i << "\t\t\t" << setprecision(10) << fixed << t << endl;
            tmr.reset();
        }
    }
    rendering(stringstream1.str(), "../test/BTreeAdd.txt");
    stringstream1.clear();
    return array;
}

vector<double> SplaySearch(SplayTree<int> &NewTree, int *Rnd) {
    std::stringstream stringstream1;
    vector<double> array;
    for (int i = 0; i < MAXSEARCH; i++) {
        Timer tmr;
        NewTree.search(Rnd[i]);
        double t = tmr.elapsed();
        array.push_back(t);
        stringstream1 << "\t\t" << Rnd[i] << "\t\t\t" << setprecision(10) << fixed << t << endl;
        tmr.reset();
    }
    rendering(stringstream1.str(), "../test/SplaySearch.txt");
    stringstream1.clear();
    return array;
}

vector<double> BTreeSearch(BTree &NewBTree, int *Rnd) {
    std::stringstream stringstream1;
    vector<double> array;
    for (int i = 0; i < MAXSEARCH; i++) {
        Timer tmr;
        NewBTree.search(Rnd[i]);
        double t = tmr.elapsed();
        array.push_back(t);
        stringstream1 << "\t\t" << Rnd[i] << "\t\t\t" << setprecision(10) << fixed << t << endl;
        tmr.reset();
    }
    rendering(stringstream1.str(), "../test/BTreeSearch.txt");
    stringstream1.clear();
    return array;
}

vector<double> BTreeDelete(BTree &NewBTree, int *Rnd) {
    std::stringstream stringstream1;
    vector<double> array;
    for (int i = 0; i < MAXSEARCH; i++) {
        Timer tmr;
        NewBTree.remove(Rnd[i]);
        double t = tmr.elapsed();
        array.push_back(t);
        stringstream1 << "\t\t" << Rnd[i] << "\t\t\t" << setprecision(10) << fixed << t << endl;
        tmr.reset();
    }
    rendering(stringstream1.str(), "../test/BTreeDelete.txt");
    stringstream1.clear();
    return array;
}

vector<double> SplayDelete(SplayTree<int> &NewTree, int *Rnd) {
    std::stringstream stringstream1;
    vector<double> array;
    for (int i = 0; i < MAXSEARCH; i++) {
        Timer tmr;
        NewTree.remove(Rnd[i]);
        double t = tmr.elapsed();
        array.push_back(t);
        stringstream1 << "\t\t" << Rnd[i] << "\t\t\t" << setprecision(10) << fixed << t << endl;
        tmr.reset();
    }
    rendering(stringstream1.str(), "../test/SplayDelete.txt");
    stringstream1.clear();
    return array;
}

vector<double> BTreeSearchMin(BTree &NewBTree) {
    std::stringstream stringstream1;
    vector<double> array;
    int min;
    for (int i = 0; i < MAXSEARCH; ++i) {
        Timer tmr;
        min = NewBTree.searchMin();
        double t = tmr.elapsed();
        array.push_back(t);
        stringstream1 << "\t\t" << min << "\t\t\t" << setprecision(10) << fixed << t << endl;
        tmr.reset();
    }
    rendering(stringstream1.str(), "../test/BTreeSearchMin.txt");
    stringstream1.clear();
    return array;
}

vector<double> SplaySearchMin(SplayTree<int> &NewTree) {
    std::stringstream stringstream1;
    vector<double> array;
    int *min;
    for (int i = 0; i < MAXSEARCH; ++i) {
        Timer tmr;
        min = NewTree.searchMin();
        double t = tmr.elapsed();
        array.push_back(t);
        stringstream1 << "\t\t" << *min << "\t\t\t" << setprecision(10) << fixed << t << endl;
        tmr.reset();
    }
    rendering(stringstream1.str(), "../test/SplaySearchMin.txt");
    stringstream1.clear();
    return array;
}

vector<double> BTreeSearchMax(BTree &NewBTree) {
    std::stringstream stringstream1;
    vector<double> array;
    int max;
    for (int i = 0; i < MAXSEARCH; ++i) {
        Timer tmr;
        max = NewBTree.searchMax();
        double t = tmr.elapsed();
        array.push_back(t);
        stringstream1 << "\t\t" << max << "\t\t\t" << setprecision(10) << fixed << t << endl;
        tmr.reset();
    }
    rendering(stringstream1.str(), "../test/BTreeSearchMax.txt");
    stringstream1.clear();
    return array;
}

vector<double> SplaySearchMax(SplayTree<int> &NewTree) {
    std::stringstream stringstream1;
    vector<double> array;
    int *max;
    for (int i = 0; i < MAXSEARCH; ++i) {
        Timer tmr;
        max = NewTree.searchMax();
        double t = tmr.elapsed();
        array.push_back(t);
        stringstream1 << "\t\t" << *max << "\t\t\t" << setprecision(10) << fixed << t << endl;
        tmr.reset();
    }
    rendering(stringstream1.str(), "../test/SplaySearchMax.txt");
    stringstream1.clear();
    return array;
}

int main(int argc, char *argv[]) {
    SplayTree<int> NewTree;
    BTree NewBTree(64);



    cout << "TEST ADD BTREE..." << endl;
    Timer tmr;
    calc(BTreeAdd(NewBTree));
    double t = tmr.elapsed();
    COUTGREEN << setprecision(10) << fixed << t << " sec" << ENDLRESET;


    cout << "TEST ADD SPLAYTREE..." << endl;
    tmr.reset();
    calc(SplayAdd(NewTree));
    t = tmr.elapsed();
    COUTGREEN << setprecision(10) << fixed << t << " sec" << ENDLRESET;

    int Rnd[MAXSEARCH];
    for (int j = 0; j < MAXSEARCH; ++j) {
        srand(rand());
        Rnd[j] = rand() % MAX;
    }

    cout << "TEST SEARCH BTREE..." << endl;
    tmr.reset();
    calc(BTreeSearch(NewBTree, Rnd));
    t = tmr.elapsed();
    COUTGREEN << setprecision(10) << fixed << t << " sec" << ENDLRESET;


    cout << "TEST SEARCH SPLAYTREE..." << endl;
    tmr.reset();
    calc(SplaySearch(NewTree, Rnd));
    t = tmr.elapsed();
    COUTGREEN << setprecision(10) << fixed << t << " sec" << ENDLRESET;


    for (int j = 0; j < MAXSEARCH; ++j) {
        srand(rand());
        Rnd[j] = rand() % MAX;
    }

    cout << "TEST DELETE BTREE..." << endl;
    tmr.reset();
    calc(BTreeDelete(NewBTree, Rnd));
    t = tmr.elapsed();
    COUTGREEN << setprecision(10) << fixed << t << " sec" << ENDLRESET;

    cout << "TEST DELETE SPLAYTREE..." << endl;
    tmr.reset();
    calc(SplayDelete(NewTree, Rnd));
    t = tmr.elapsed();
    COUTGREEN << setprecision(10) << fixed << t << " sec" << ENDLRESET;


    cout << "TEST MIN BTREE..." << endl;
    tmr.reset();
    calc(BTreeSearchMin(NewBTree));
    t = tmr.elapsed();
    COUTGREEN << setprecision(10) << fixed << t << " sec" << ENDLRESET;


    cout << "TEST MIN SPLAYTREE..." << endl;
    tmr.reset();
    calc(SplaySearchMin(NewTree));
    t = tmr.elapsed();
    COUTGREEN << setprecision(10) << fixed << t << " sec" << ENDLRESET;


    cout << "TEST MAX BTREE..." << endl;
    tmr.reset();
    calc(BTreeSearchMax(NewBTree));
    t = tmr.elapsed();
    COUTGREEN << setprecision(10) << fixed << t << " sec" << ENDLRESET;


    cout << "TEST MAX SPLAYTREE..." << endl;
    tmr.reset();
    calc(SplaySearchMax(NewTree));
    t = tmr.elapsed();
    COUTGREEN << setprecision(10) << fixed << t << " sec" << ENDLRESET;
    tmr.reset();

    return 0;
}