#ifndef SAODCURS_SPLAYTREE_H
#define SAODCURS_SPLAYTREE_H

#include <iostream>
#include <memory>

using namespace std;

template<typename TYPE>
class SplayTree {
public:
    struct Node {
        int key;
        TYPE value;
        shared_ptr<Node> LeftNode;
        shared_ptr<Node> RightNode;
        shared_ptr<Node> ParentNode;

        Node(int _key, TYPE _value) :
                key(_key), value(_value), LeftNode(NULL), RightNode(NULL), ParentNode(NULL) {};
        ~Node() {
            this->LeftNode = NULL;
            this->RightNode = NULL;
        };

        bool Comparison(shared_ptr<Node> Tree) {
            if ((this->LeftNode && !Tree->LeftNode) || (this->RightNode && !Tree->RightNode))
                return false;
            if ((!this->LeftNode && Tree->LeftNode) || (!this->RightNode && Tree->RightNode))
                return false;
            bool compLeft = true,
                    complRight = true;
            if ((this->value != Tree->value) || (this->key != Tree->key))
                return false;
            else {
                if (!this->LeftNode && !this->RightNode)
                    return true;
                if (this->LeftNode)
                    compLeft = this->LeftNode->Comparison(Tree->LeftNode);
                if (this->RightNode)
                    complRight = this->RightNode->Comparison(Tree->RightNode);
                return compLeft && complRight;
            }
        }
    };
    shared_ptr<Node> RootNode;
public:
    SplayTree() :
            RootNode(NULL) {};
    ~SplayTree() = default;

    bool insert(int key, TYPE &value) {
        bool place = false;
        if (!this->RootNode) {
            this->RootNode = make_shared<Node>(key, value);
            return true;
        }
        shared_ptr<Node> thisNode = this->RootNode;
        while (!place) {
            if (key == thisNode->key)
                return false;
            if (key < thisNode->key) {
                if (!thisNode->LeftNode) {
                    thisNode->LeftNode = make_shared<Node>(key, value);
                    thisNode->LeftNode->ParentNode = thisNode;
                    thisNode = thisNode->LeftNode;
                    place = true;
                } else
                    thisNode = thisNode->LeftNode;
            } else if (!thisNode->RightNode) {
                thisNode->RightNode = make_shared<Node>(key, value);
                thisNode->RightNode->ParentNode = thisNode;
                thisNode = thisNode->RightNode;
                place = true;
            } else
                thisNode = thisNode->RightNode;
        }
        splay(thisNode);
        this->RootNode = thisNode;
        return place;
    };
    bool remove(int key) {
        if (search(key)) {
            search(key);
            if (!this->RootNode->RightNode) {
                this->RootNode = this->RootNode->LeftNode;
                if (this->RootNode)
                    this->RootNode->ParentNode = NULL;
                return true;
            }
            if (!merge(this->RootNode->LeftNode, this->RootNode->RightNode)) {
                this->RootNode = this->RootNode->RightNode;
                if (this->RootNode)
                    this->RootNode->ParentNode = NULL;
                return true;
            }
            return true;
        } else
            return false;
    }
    TYPE *search(int key) {
        if (!this->RootNode)
            return NULL;
        shared_ptr<Node> tempRootNode = this->RootNode;
        while (true) {
            if (key == tempRootNode->key) {
                splay(tempRootNode);
                this->RootNode = tempRootNode;
                return &tempRootNode->value;
            } else if (key < tempRootNode->key)
                if (tempRootNode->LeftNode)
                    tempRootNode = tempRootNode->LeftNode;
                else {
                    splay(tempRootNode);
                    this->RootNode = tempRootNode;
                    return NULL;
                }
            else {
                if (tempRootNode->RightNode)
                    tempRootNode = tempRootNode->RightNode;
                else {
                    splay(tempRootNode);
                    this->RootNode = tempRootNode;
                    return NULL;
                }
            }
        }
    };
    TYPE* searchMax() {
        if (!this->RootNode)
            return NULL;
        shared_ptr<Node> temp = this->RootNode;
        while (temp->RightNode)
            temp = temp->RightNode;
        return &temp->value;
    }
    TYPE* searchMin() {
        if (!this->RootNode)
            return NULL;
        shared_ptr<Node> temp = this->RootNode;
        while (temp->LeftNode)
            temp = temp->LeftNode;
        return &temp->value;
    }
    void turnLeft(shared_ptr<Node> NodeArg) {
        shared_ptr<Node> temp = NodeArg->LeftNode;
        if (!NodeArg->ParentNode)
            this->RootNode = temp;
        else if (NodeArg == NodeArg->ParentNode->LeftNode)
            NodeArg->ParentNode->LeftNode = temp;
        else
            NodeArg->ParentNode->RightNode = temp;
        temp->ParentNode = NodeArg->ParentNode;
        NodeArg->LeftNode = temp->RightNode;
        if (NodeArg->LeftNode)
            NodeArg->LeftNode->ParentNode = NodeArg;
        temp->RightNode = NodeArg;
        NodeArg->ParentNode = temp;
    }
    void turnRight(shared_ptr<Node> NodeArg) {
        shared_ptr<Node> temp = NodeArg->RightNode;
        if (!NodeArg->ParentNode)
            this->RootNode = temp;
        else if (NodeArg == NodeArg->ParentNode->LeftNode)
            NodeArg->ParentNode->LeftNode = temp;
        else
            NodeArg->ParentNode->RightNode = temp;
        temp->ParentNode = NodeArg->ParentNode;
        NodeArg->RightNode = temp->LeftNode;
        if (NodeArg->RightNode)
            NodeArg->RightNode->ParentNode = NodeArg;
        temp->LeftNode = NodeArg;
        NodeArg->ParentNode = temp;
    }
    void splay(shared_ptr<Node> NodeArg) {
        if (!NodeArg)
            return;
        while (NodeArg->ParentNode && NodeArg->ParentNode->ParentNode) {
            if (NodeArg == NodeArg->ParentNode->LeftNode) {
                if (NodeArg->ParentNode == NodeArg->ParentNode->ParentNode->LeftNode) {
                    // Zig-Zig
                    turnLeft(NodeArg->ParentNode->ParentNode);
                    turnLeft(NodeArg->ParentNode);
                } else {
                    // Zig-Zag
                    turnLeft(NodeArg->ParentNode);
                    turnRight(NodeArg->ParentNode);
                }
            } else {
                if (NodeArg->ParentNode == NodeArg->ParentNode->ParentNode->RightNode) {
                    // Zig-Zig
                    turnRight(NodeArg->ParentNode->ParentNode);
                    turnRight(NodeArg->ParentNode);
                } else {
                    // Zig-Zag
                    turnRight(NodeArg->ParentNode);
                    turnLeft(NodeArg->ParentNode);
                }
            }
        }
        if (!NodeArg->ParentNode)
            return;
        if (NodeArg == NodeArg->ParentNode->LeftNode) {
            // Zig
            turnLeft(NodeArg->ParentNode);
        } else
            // Zig
            turnRight(NodeArg->ParentNode);
    }
    bool merge(shared_ptr<Node> TreeLeft, shared_ptr<Node> TreeRight) {
        shared_ptr<Node> RootNew = mergeMax(TreeLeft);
        if (!RootNew)
            return false;
        splay(RootNew);
        RootNew->RightNode = TreeRight;
        if (TreeRight)
            TreeRight->ParentNode = RootNew;
        return true;
    }
    auto mergeMax(shared_ptr<Node> TreeLeft) const -> shared_ptr<Node> {
        if (!TreeLeft)
            return NULL;
        shared_ptr<Node> temp = TreeLeft;
        while (temp->RightNode)
            temp = temp->RightNode;
        return temp;
    }
    bool operator==(const SplayTree &Tree) {
        return (RootNode->Comparison(Tree.RootNode));
    };
};

#endif //SAODCURS_SPLAYTREE_H
