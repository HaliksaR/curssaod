#ifndef SAODCURS_BTREE_H
#define SAODCURS_BTREE_H

#include <iostream>
#include <memory>
#include <fstream>

using namespace std;

class Node {
    friend class BTree;

public:
    int *keys;
    int degree;
    int keysNumber;
    bool isLeaf;
    Node **ChildArray;
public:

    Node(int degree_, bool isLeaf_) {
        degree = degree_;
        isLeaf = isLeaf_;
        keys = new int[2 * degree - 1];
        ChildArray = new Node *[2 * degree];
        keysNumber = 0;
    }

    int findKey(int key) {
        int counter = 0;
        while (counter < keysNumber && keys[counter] < key)
            counter++;
        return counter;
    }

    void remove(int key) {
        int find = findKey(key);
        if (find < keysNumber && keys[find] == key) {
            if (isLeaf)
                removeFromLeaf(find);
            else
                removeFromNonLeaf(find);
        } else {
            if (isLeaf)
                return;
            bool flag = (find == keysNumber);
            if (ChildArray[find]->keysNumber < degree)
                fill(find);
            if (flag && find > keysNumber)
                ChildArray[find - 1]->remove(key);
            else
                ChildArray[find]->remove(key);
        }
    }

    void removeFromLeaf(int idx) {
        for (int i = idx + 1; i < keysNumber; i++)
            keys[i - 1] = keys[i];
        keysNumber--;
    }

    void removeFromNonLeaf(int idx) {
        int key = keys[idx];
        if (ChildArray[idx]->keysNumber >= degree) {
            int pred = getPred(idx);
            keys[idx] = pred;
            ChildArray[idx]->remove(pred);
        } else if (ChildArray[idx + 1]->keysNumber >= degree) {
            int succ = getSucc(idx);
            keys[idx] = succ;
            ChildArray[idx + 1]->remove(succ);
        } else {
            merge(idx);
            ChildArray[idx]->remove(key);
        }
    }

    int getPred(int idx) {
        Node *Node = ChildArray[idx];
        while (!Node->isLeaf)
            Node = Node->ChildArray[Node->keysNumber];
        return Node->keys[Node->keysNumber - 1];
    }

    int getSucc(int idx) {
        Node *Node = ChildArray[idx + 1];
        while (!Node->isLeaf)
            Node = Node->ChildArray[0];
        return Node->keys[0];
    }

    void fill(int idx) {
        if (idx != 0 && ChildArray[idx - 1]->keysNumber >= degree)
            borrowFromPrev(idx);
        else if (idx != keysNumber && ChildArray[idx + 1]->keysNumber >= degree)
            borrowFromNext(idx);
        else {
            if (idx != keysNumber)
                merge(idx);
            else
                merge(idx - 1);
        }
    }

    void borrowFromPrev(int idx) {
        Node *ChildNode = ChildArray[idx];
        Node *SiblingNode = ChildArray[idx - 1];
        for (int i = ChildNode->keysNumber - 1; i >= 0; i--)
            ChildNode->keys[i + 1] = ChildNode->keys[i];
        if (!ChildNode->isLeaf) {
            for (int i = ChildNode->keysNumber; i >= 0; i--)
                ChildNode->ChildArray[i + 1] = ChildNode->ChildArray[i];
        }
        ChildNode->keys[0] = keys[idx - 1];
        if (!ChildNode->isLeaf)
            ChildNode->ChildArray[0] = SiblingNode->ChildArray[SiblingNode->keysNumber];
        keys[idx - 1] = SiblingNode->keys[SiblingNode->keysNumber - 1];
        ChildNode->keysNumber += 1;
        SiblingNode->keysNumber -= 1;
    }

    void borrowFromNext(int idx) {
        Node *ChildNode = ChildArray[idx];
        Node *SiblingNode = ChildArray[idx + 1];
        ChildNode->keys[(ChildNode->keysNumber)] = keys[idx];
        if (!(ChildNode->isLeaf))
            ChildNode->ChildArray[(ChildNode->keysNumber) + 1] = SiblingNode->ChildArray[0];
        keys[idx] = SiblingNode->keys[0];
        for (int i = 1; i < SiblingNode->keysNumber; i++)
            SiblingNode->keys[i - 1] = SiblingNode->keys[i];
        if (!SiblingNode->isLeaf)
            for (int i = 1; i <= SiblingNode->keysNumber; i++)
                SiblingNode->ChildArray[i - 1] = SiblingNode->ChildArray[i];
        ChildNode->keysNumber += 1;
        SiblingNode->keysNumber -= 1;
    }

    void merge(int idx) {
        Node *ChildNode = ChildArray[idx];
        Node *SiblingNode = ChildArray[idx + 1];
        ChildNode->keys[degree - 1] = keys[idx];
        for (int i = 0; i < SiblingNode->keysNumber; i++)
            ChildNode->keys[i + degree] = SiblingNode->keys[i];
        if (!ChildNode->isLeaf)
            for (int i = 0; i <= SiblingNode->keysNumber; i++)
                ChildNode->ChildArray[i + degree] = SiblingNode->ChildArray[i];
        for (int i = idx + 1; i < keysNumber; i++)
            keys[i - 1] = keys[i];
        for (int i = idx + 2; i <= keysNumber; i++)
            ChildArray[i - 1] = ChildArray[i];
        ChildNode->keysNumber += SiblingNode->keysNumber + 1;
        keysNumber--;
        delete (SiblingNode);
    }

    void insertNonFull(int key) {
        int number = keysNumber - 1;
        if (isLeaf) {
            while (number >= 0 && keys[number] > key) {
                keys[number + 1] = keys[number];
                number--;
            }
            keys[number + 1] = key;
            keysNumber = keysNumber + 1;
        } else {
            while (number >= 0 && keys[number] > key)
                number--;
            if (ChildArray[number + 1]->keysNumber == 2 * degree - 1) {
                splitChild(number + 1, ChildArray[number + 1]);
                if (keys[number + 1] < key)
                    number++;
            }
            ChildArray[number + 1]->insertNonFull(key);
        }
    }

    void splitChild(int number, Node *NodeArg) {
        Node *node = new Node(NodeArg->degree, NodeArg->isLeaf);
        node->keysNumber = degree - 1;
        for (int j = 0; j < degree - 1; j++)
            node->keys[j] = NodeArg->keys[j + degree];
        if (!NodeArg->isLeaf)
            for (int j = 0; j < degree; j++)
                node->ChildArray[j] = NodeArg->ChildArray[j + degree];
        NodeArg->keysNumber = degree - 1;
        for (int j = keysNumber; j >= number + 1; j--)
            ChildArray[j + 1] = ChildArray[j];
        ChildArray[number + 1] = node;
        for (int j = keysNumber - 1; j >= number; j--)
            keys[j + 1] = keys[j];
        keys[number] = NodeArg->keys[degree - 1];
        keysNumber = keysNumber + 1;
    }

    Node *search(int key) {
        int i = 0;
        while (i < keysNumber && key > keys[i])
            i++;
        if (keys[i] == key)
            return this;
        if (isLeaf)
            return NULL;
        return ChildArray[i]->search(key);
    }

};

class BTree {
    Node *Root;
    int degree;
public:
    BTree(int degree_) {
        Root = NULL;
        degree = degree_;
    }

    Node *search(int key) {
        return (!Root) ? NULL : Root->search(key);
    }

    int searchMax() {
        Node *RightNode = Root;
        while (!RightNode->isLeaf)
            RightNode = RightNode->ChildArray[RightNode->keysNumber];
        return RightNode->keys[RightNode->keysNumber - 1];
    }

    int searchMin() {
        Node *LeftNode = Root;
        while (!LeftNode->isLeaf)
            LeftNode = LeftNode->ChildArray[0];
        return LeftNode->keys[0];
    }

    void insert(int key) {
        if (!Root) {
            Root = new Node(degree, true);
            Root->keys[0] = key;
            Root->keysNumber = 1;
        } else {
            if (Root->keysNumber == 2 * degree - 1) {
                Node *changeRoot = new Node(degree, false);
                changeRoot->ChildArray[0] = Root;
                changeRoot->splitChild(0, Root);
                int i = 0;
                if (changeRoot->keys[0] < key)
                    i++;
                changeRoot->ChildArray[i]->insertNonFull(key);
                Root = changeRoot;
            } else
                Root->insertNonFull(key);
        }
    }

    void remove(int key) {
        if (!Root)
            return;
        Root->remove(key);
        if (Root->keysNumber == 0) {
            Node *tmp = Root;
            if (Root->isLeaf)
                Root = NULL;
            else
                Root = Root->ChildArray[0];
            delete tmp;
        }
    }
};

#endif
