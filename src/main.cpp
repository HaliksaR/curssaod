#include <iostream>
#include <cstdlib>
#include "includes/SplayTree.h"
#include "includes/BTree.h"

using namespace std;

int main(int argc, char *argv[]) {
    SplayTree<int> Stree;
    int val = 4;
    BTree btree(64);
    Stree.insert(6, val);
    Stree.insert(12, val);
    Stree.insert(44, val);
    Stree.insert(2, val);
    Stree.insert(6, val);
    Stree.remove(44);

    btree.insert(45);
    btree.insert(5);
    btree.insert(545);
    btree.insert(4565);
    btree.insert(4554);
    btree.insert(4544);
    btree.remove(5);

    return 0;
}

